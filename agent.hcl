pid_file = "./pidfile"

vault {
  address = "https://127.0.0.1:8200"
}

auto_auth {
  method {
    type = "approle"

    config = {
      role_id_file_path = "vault/roleid"
      secret_id_file_path = "vault/secretid"
    }
  }

  sink {
    type = "file"
    config = {
      path = "vault/token"
    }
  }
}

template {
  source      = "config/template.ctmpl"
  destination = "config/render.txt"
}
template_config {
  static_secret_render_interval = 1m
}
