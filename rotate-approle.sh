#!/bin/bash
# Make sure and store VAULT_TOKEN and VAULT_ADDR as environment variables.
VAULT_TOKEN="s.OPiLbNph0b0N3f8tlbgMdso6"
VAULT_ADDR="http://localhost:8200"

# Renew our token before we do anything else.
curl -sS --fail -X POST -H "X-Vault-Token: $VAULT_TOKEN" ${VAULT_ADDR}/v1/auth/token/renew-self | grep -q 'lease_duration'
retval=$?
if [[ $retval -ne 0 ]]; then
  echo "Error renewing Vault token lease."
fi

# generate value
foo=$(curl -sS --fail -X POST -H "X-Vault-Token: $VAULT_TOKEN" -H "Content-Type: application/json" --data '{"format":"hex"}'  ${VAULT_ADDR}/v1/sys/tools/random/10 | jq -r '.data|.random_bytes')

# update value
curl -sS --fail -X POST -H "X-Vault-Token: $VAULT_TOKEN" -H "Content-Type: application/json" --data '{"data":{"foo":"'$foo'"}}' ${VAULT_ADDR}/v1/secret/data/my-secret | grep -q 'request_id'
if [[ $retval -ne 0 ]]; then
  echo "Error update value"
  else 
  echo "Success update value"
fi
