# DEMO Vault A2A 

![alt topology](./topology.png?raw=true)


## Reference
- https://www.vaultproject.io/docs/concepts/policies
- https://www.vaultproject.io/docs/auth/approle
- https://www.vaultproject.io/api/auth/approle#token_policies
- https://www.vaultproject.io/docs/agent/template



## Environment
- server deployment 34.101.236.16
- export VAULT_ADDR=http://127.0.0.1:8200
- export VAULT_TOKEN=s.nWJGXpECaWGgLrKknYdKgEmQ


## How To


1. enable and create kv secret
    ```
    vault secrets enable -version=2 kv
    vault kv put secret/my-secret apikey=s3cr3t 
    ```

2. create policy
    file policy-secret-read.hcl

    ```
    path "secret/*" {
      capabilities = ["read", "list"]
    }

    vault policy write policy-secret-read policy-secret-read.hcl
    ```


3.  enable and create approle 
    ```
    vault auth enable approle

    vault write auth/approle/role/my-role  token_no_default_policy=true token_policies="policy-secret-read" secret_id_num_uses=0 secret_id_ttl=0 token_ttl=2m token_max_ttl=0 secret_id_bound_cidrs="127.0.0.1/32" token_bound_cidrs="127.0.0.1/32"
    ```

4.  get role_id, create secret id 
    ``` 
    export ROLE_ID="$(vault read -field=role_id auth/approle/role/my-role/role-id)"
    echo $ROLE_ID > vault/roleid 
    export SECRET_ID="$(vault write -f -field=secret_id auth/approle/role/my-role/secret-id)"
    echo $SECRET_ID > vault/secretid 
    #vault write auth/approle/login role_id="$(cat vault/roleid)" secret_id="$(cat vault/secretid)"
    vault write auth/approle/login role_id="$(vault read -field=role_id auth/approle/role/my-role/role-id)" secret_id="$(vault write -f -field=secret_id auth/approle/role/my-role/secret-id)"
    ```


5.  create token
    ``` vault write auth/approle/login role_id="$ROLE_ID" secret_id="$SECRET_ID"```

6.  check to read secret 
    ```
    export VAULT_TOKEN="{{token from no.5}}"
    vault kv get -format json -field=data secret/my-secret
    ```

7.  running agent 
    ``` vault agent -config=agent.hcl ```


8.  run with envconsul 
    ```
    envconsul -vault-renew-token=false -vault-agent-token-file="vault/token" -vault-addr="http://localhost:8200" -secret="/secret/my-secret" env PORT=12000 ./client ```

    #test check variable
    http://34.101.236.16:12000/



# Run Demo
    ```
    start agent
    ./agent.sh s.nWJGXpECaWGgLrKknYdKgEmQ
    ./cron-rotate.sh
    envconsul -vault-renew-token=false -vault-agent-token-file="vault/token" -vault-addr="http://localhost:8200" -secret="/secret/my-secret" env PORT=12000 ./client
    envconsul -vault-renew-token=false -vault-agent-token-file="vault/token" -vault-addr="http://localhost:8200" -secret="/secret/my-secret" env envoy -c envoy.yaml
  
    call api will simulate call from client port 12000 to envoy port 11000
    http://34.101.236.16:12000/secret_my_secret_apikey?call=http://34.101.236.16:11000/headers
    ```

