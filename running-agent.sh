#!/bin/bash

export VAULT_ADDR=http://127.0.0.1:8200
VAULT_TOKEN=$1

echo "$(vault read -field=role_id auth/approle/role/my-role/role-id)" > vault/roleid 
echo "$(vault write -f -field=secret_id auth/approle/role/my-role/secret-id)" > vault/secretid 

vault agent -config=agent.hcl

