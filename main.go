package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	//go initSignals()

	port := "12000"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}

	http.HandleFunc("/", HelloServer)
	fmt.Println("running.." + port)
	http.ListenAndServe(":"+port, nil)

}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	keyval := r.URL.Path[1:]
	if keyval != "" {
		fmt.Fprintf(w, "variable %s=%s", keyval, os.Getenv(keyval))

		if r.URL.Query().Get("call") != "" {

			respBody := callHTTP(os.Getenv(keyval), r.URL.Query().Get("call"))
			fmt.Fprintf(w, "\nrequest %s", r.URL.Query().Get("call"))
			fmt.Fprintf(w, "\nresponse %s", respBody)
		}

		return
	}
	environ := ""
	for _, pair := range os.Environ() {
		//fmt.Println(pair)
		environ += pair + "\n"

	}
	fmt.Fprintf(w, "list-environment-variable, %s!", environ)

}

func callHTTP(apikey, url string) string {
	client := &http.Client{}
	req, err1 := http.NewRequest("GET", url, nil)
	req.Header.Set("apikey", apikey)
	res, err2 := client.Do(req)
	// check for response error
	if err1 != nil || err2 != nil {
		fmt.Println("error-http")
	}
	// read response data
	data, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()
	return string(data)

}

// initialize signal handler
func initSignals() {
	var captureSignal = make(chan os.Signal, 1)
	signal.Notify(captureSignal, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
	signalHandler(<-captureSignal)
}

// signal handler
func signalHandler(signal os.Signal) {
	fmt.Printf("\nCaught signal: %+v", signal)
	fmt.Println("\nWait for 3 second to finish processing")
	time.Sleep(3 * time.Second)

	switch signal {

	case syscall.SIGHUP: // kill -SIGHUP XXXX
		fmt.Println("- got hungup")

	case syscall.SIGINT: // kill -SIGINT XXXX or Ctrl+c
		fmt.Println("- got SIGINT ")

	case syscall.SIGTERM: // kill -SIGTERM XXXX
		fmt.Println("- got force stop")

	case syscall.SIGQUIT: // kill -SIGQUIT XXXX
		fmt.Println("- stop and core dump")

	case syscall.SIGKILL: // kill -SIGQUIT XXXX
		fmt.Println("- kill")

	default:
		fmt.Println("- unknown signal")
	}

	fmt.Println("\nFinished server cleanup")
	os.Exit(0)

}
